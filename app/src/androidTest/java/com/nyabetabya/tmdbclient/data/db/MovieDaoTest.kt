package com.nyabetabya.tmdbclient.data.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth
import com.nyabetabya.findmymovie.data.db.MovieDao
import com.nyabetabya.findmymovie.data.db.TMDBDatabase
import com.nyabetabya.findmymovie.data.model.movie.Movie
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule

import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MovieDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var dao: MovieDao
    private lateinit var database: TMDBDatabase


    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            TMDBDatabase::class.java
        ).build()
        dao = database.movieDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun saveMoviesTest() = runBlocking {
        val movies = listOf(
            Movie(
                1, true, "mature", "es",
                "originalTitle1", "overview1", 4.5,
                "posterPath1", "releaseDate1", "title1",
                true, 4.5, 100
            ),
            Movie(
                2, true, "mature", "es",
                "originalTitle2", "overview2", 4.5,
                "posterPath2", "releaseDate2", "title2",
                true, 4.5, 100
            ),
            Movie(
                3, true, "mature", "es",
                "originalTitle3", "overview3", 4.5,
                "posterPath3", "releaseDate3", "title3",
                true, 4.5, 100
            )
        )

//        dao.saveMovies(movies)
//
//        val allMovies = dao.getMovies()
//
//        Truth.assertThat(allMovies).isEqualTo(movies)
    }

    @Test
    fun deleteMoviesTest() = runBlocking{
        val movies = listOf(
            Movie(
                1, true, "mature", "es",
                "originalTitle1", "overview1", 4.5,
                "posterPath1", "releaseDate1", "title1",
                true, 4.5, 100
            ),
            Movie(
                2, true, "mature", "es",
                "originalTitle2", "overview2", 4.5,
                "posterPath2", "releaseDate2", "title2",
                true, 4.5, 100
            ),
            Movie(
                3, true, "mature", "es",
                "originalTitle3", "overview3", 4.5,
                "posterPath3", "releaseDate3", "title3",
                true, 4.5, 100
            )
        )

//        dao.saveMovies(movies)
//        dao.deleteAllMovies()
//
//        val resultMovies = dao.getMovies()
//
//        Truth.assertThat(resultMovies).isEmpty()
    }
}