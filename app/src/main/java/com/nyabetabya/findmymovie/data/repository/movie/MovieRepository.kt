package com.nyabetabya.findmymovie.data.repository.movie

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.nyabetabya.findmymovie.data.api.TMDBService
import com.nyabetabya.findmymovie.data.db.TMDBDatabase
import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.mediator.MoviesRemoteMediator
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

private const val QUERY_POPULAR_SEARCH = "popular"

class MovieRepository @Inject constructor(
    private val tmdbService: TMDBService,
    private val tmdbDatabase: TMDBDatabase
) {
    private val movieDao = tmdbDatabase.movieDao()

    fun getPopularMoviesPaged(): Flow<PagingData<Movie>> =
        Pager(
            config = PagingConfig(pageSize = 20, maxSize = 500),
            remoteMediator = MoviesRemoteMediator(QUERY_POPULAR_SEARCH, tmdbService, tmdbDatabase),
            pagingSourceFactory = { movieDao.getSearchResultMoviesPaged(QUERY_POPULAR_SEARCH) }
        ).flow

    fun getSearchResultsPaged(query: String): Flow<PagingData<Movie>> =
        Pager(
            config = PagingConfig(pageSize = 20, maxSize = 200),
            remoteMediator = MoviesRemoteMediator(query, tmdbService, tmdbDatabase),
            pagingSourceFactory = { movieDao.getSearchResultMoviesPaged(query) }
        ).flow
}