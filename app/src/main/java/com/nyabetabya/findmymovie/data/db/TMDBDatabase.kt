package com.nyabetabya.findmymovie.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nyabetabya.findmymovie.data.model.search.SearchQueryRemoteKey

import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.data.model.movie.SearchResult

@Database(
    entities = [Movie::class, SearchResult::class, SearchQueryRemoteKey::class],
    version = 1
)
abstract class TMDBDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun searchQueryRemoteKeyDao(): SearchQueryRemoteKeyDao
}