package com.nyabetabya.findmymovie.data.db

import androidx.paging.PagingSource
import androidx.room.*
import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.data.model.movie.SearchResult

@Dao
interface MovieDao {

    @Query("SELECT * FROM search_result INNER JOIN movies ON movieId = id WHERE searchQuery = :query  ORDER BY queryPosition")
    fun getSearchResultMoviesPaged(query: String): PagingSource<Int, Movie>

    @Query("SELECT MAX(queryPosition) FROM search_result WHERE searchQuery = :searchQuery")
    suspend fun getLastQueryPosition(searchQuery: String): Int?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovies(movies: List<Movie>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSearchResults(searchResults: List<SearchResult>)

    @Query("DELETE FROM search_query_remote_keys WHERE searchQuery = :query")
    suspend fun deleteSearchResultsForQuery(query: String)

    @Query("DELETE FROM movies WHERE id IN (SELECT id FROM movies INNER JOIN search_result ON movieId = id WHERE searchQuery = :query)")
    suspend fun deleteMovies(query: String)
}