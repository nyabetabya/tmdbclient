package com.nyabetabya.findmymovie.data.api

import com.nyabetabya.findmymovie.BuildConfig
import com.nyabetabya.findmymovie.data.model.movie.MovieList
import retrofit2.http.GET
import retrofit2.http.Query

interface TMDBService {

    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val API_KEY = BuildConfig.TMDB_API_ACCESS_KEY
    }

    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("api_key") apiKey: String = API_KEY,
        @Query("language") language: String = "es-MX",
        @Query("page") page: String,
        @Query("include_adult") includeAdult: Boolean = false
    ): MovieList

    @GET("search/movie")
    suspend fun searchMovies(
        @Query("api_key") apiKey: String = API_KEY,
        @Query("query") query: String,
        @Query("page") page: String,
        @Query("language") language: String = "es-MX",
        @Query("include_adult") includeAdult: Boolean = false
    ): MovieList
}