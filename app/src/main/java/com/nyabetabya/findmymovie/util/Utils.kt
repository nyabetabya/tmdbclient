package com.nyabetabya.findmymovie.util

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.snackbar.Snackbar
import com.nyabetabya.findmymovie.R

fun Fragment.showSnackbar(
    message: String,
    duration: Int = Snackbar.LENGTH_LONG,
    view: View = requireView()
) {
    Snackbar.make(view, message, duration).show()
}

inline fun <T: View> T.showIfOrInvisible(condition : (T) -> Boolean){
    if(condition(this)){
        this.visibility = View.VISIBLE
    }else{
        this.visibility = View.INVISIBLE
    }
}

inline fun SearchView.onQueryTextSubmit(crossinline listener : (String) -> Unit){
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
        override fun onQueryTextSubmit(query: String?): Boolean {
            if(!query.isNullOrBlank()){
                listener(query)
            }
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            return true
        }
    })
}

fun Fragment.addFragment(
    fm: FragmentManager,
    fragment: Fragment,
    container: Int,
    replace: Boolean,
    addToBackStack: Boolean,
    addAnimation: Boolean
) {
    val fragmentTransaction = fm.beginTransaction()
    if (addAnimation) {
        fragmentTransaction.setCustomAnimations(
            R.anim.anim_slide_in_right,
            R.anim.anim_slide_out_left,
            R.anim.anim_slide_in_left,
            R.anim.anim_slide_out_right
        )
    }
    if (replace) {
        fragmentTransaction.replace(container, fragment, fragment.javaClass.name)
    } else {
        fragmentTransaction.add(container, fragment, fragment.javaClass.name)
    }

    if (addToBackStack) {
        fragmentTransaction.addToBackStack(fragment.javaClass.name)
    }

    fragmentTransaction.commit()
}