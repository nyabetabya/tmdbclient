package com.nyabetabya.findmymovie.mediator

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.nyabetabya.findmymovie.data.api.TMDBService
import com.nyabetabya.findmymovie.data.db.TMDBDatabase
import com.nyabetabya.findmymovie.data.model.search.SearchQueryRemoteKey
import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.data.model.movie.SearchResult
import retrofit2.HttpException
import java.io.IOException

private const val NEWS_STARING_PAGE_INDEX = 1

@ExperimentalPagingApi
class MoviesRemoteMediator(
    private val searchQuery: String,
    private val tmdbService: TMDBService,
    private val tmdbDatabase: TMDBDatabase
) : RemoteMediator<Int, Movie>() {

    private val movieDao = tmdbDatabase.movieDao()
    private val searchQueryRemoteKeyDao = tmdbDatabase.searchQueryRemoteKeyDao()

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, Movie>
    ): MediatorResult {
        val page = when (loadType) {
            LoadType.REFRESH -> NEWS_STARING_PAGE_INDEX
            LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
            LoadType.APPEND -> searchQueryRemoteKeyDao.getRemoteKey(searchQuery).nextPageKey
        }

        try {
            val response = if (searchQuery == "popular") {
                tmdbService.getPopularMovies(
                    page = page.toString()
                )
            } else {
                tmdbService.searchMovies(
                    page = page.toString(),
                    query = searchQuery
                )
            }

            val serverSearchResults = response.movies

            val searchResultMovies = serverSearchResults.filter {
                it.posterPath != null
            }

            tmdbDatabase.withTransaction {
                var lastQueryPosition = movieDao.getLastQueryPosition(searchQuery) ?: 0

                if (loadType == LoadType.REFRESH) {
                    movieDao.deleteMovies(searchQuery)
                    movieDao.deleteSearchResultsForQuery(searchQuery)
                    lastQueryPosition = 0
                }

                var queryPosition = lastQueryPosition + 1

                val searchResult = searchResultMovies.map { movie ->
                    SearchResult(
                        searchQuery,
                        movie.id,
                        queryPosition++
                    )
                }

                val nextPageKey = page + 1

                movieDao.insertMovies(searchResultMovies)
                movieDao.insertSearchResults(searchResult)
                searchQueryRemoteKeyDao.insertRemoteKey(
                    SearchQueryRemoteKey(searchQuery, nextPageKey)
                )
            }
            return MediatorResult.Success(
                endOfPaginationReached = searchResultMovies.isEmpty()
            )

        } catch (e: IOException) {
            return MediatorResult.Error(e)
        } catch (e: HttpException) {
            return MediatorResult.Error(e)
        }
    }
}