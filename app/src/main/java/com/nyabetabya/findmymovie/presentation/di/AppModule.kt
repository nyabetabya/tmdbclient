package com.nyabetabya.findmymovie.presentation.di

import android.app.Application
import androidx.room.Room
import com.nyabetabya.findmymovie.data.api.TMDBService
import com.nyabetabya.findmymovie.data.db.TMDBDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(TMDBService.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideNewsApi(retrofit: Retrofit): TMDBService =
        retrofit.create(TMDBService::class.java)

    @Provides
    @Singleton
    fun provideDatabase(app: Application): TMDBDatabase =
        Room.databaseBuilder(app, TMDBDatabase::class.java, "news_movies_database")
            .fallbackToDestructiveMigration()
            .build()
}