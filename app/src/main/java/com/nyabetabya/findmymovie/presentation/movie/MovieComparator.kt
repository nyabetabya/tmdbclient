package com.nyabetabya.findmymovie.presentation.movie

import androidx.recyclerview.widget.DiffUtil
import com.nyabetabya.findmymovie.data.model.movie.Movie

class MovieComparator : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
        oldItem == newItem
}