package com.nyabetabya.findmymovie.presentation.movie

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.SearchView
import androidx.activity.addCallback
import androidx.core.view.isVisible

import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.nyabetabya.findmymovie.R
import com.nyabetabya.findmymovie.databinding.FragmentMovieBinding
import com.nyabetabya.findmymovie.presentation.movie.detail.DetailMovieFragment
import com.nyabetabya.findmymovie.util.showIfOrInvisible
import com.nyabetabya.findmymovie.util.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter

@AndroidEntryPoint
class MovieFragment : Fragment(R.layout.fragment_movie) {

    private val viewModel: MovieViewModel by viewModels()
    private var currentBinding: FragmentMovieBinding? = null
    private val binding get() = currentBinding!!

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        currentBinding = FragmentMovieBinding.bind(view)

        initRecyclerView()
        initSearchView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (viewModel.isAnyQuery) {
                viewModel.isAnyQuery = false
                binding.searchMovie.clearFocus()
                binding.searchMovie.setQuery("", false)
                viewModel.onSearchQuerySubmit(null)
                isEnabled = false
            }else {
                activity?.finish()
               // activity?.supportFragmentManager?.beginTransaction()?.remove(this@MovieFragment)?.commit()
            }
        }
    }

    @SuppressLint("StringFormatInvalid")
    @InternalCoroutinesApi
    private fun initRecyclerView() {
        val movieAdapter = MovieAdapter(
            onItemClick = { movie ->
                val bundle = Bundle()
                bundle.putParcelable("movie", movie)

                val detailMovieFragment = DetailMovieFragment()
                detailMovieFragment.arguments = bundle

                val action = MovieFragmentDirections.actionMovieFragmentToDetailMovieFragment(movie)
                findNavController().navigate(action)

//                activity?.let {
//                    addFragment(
//                        it.supportFragmentManager,
//                        detailMovieFragment,
//                        android.R.id.content,
//                        replace = true,
//                        addToBackStack = true,
//                        addAnimation = true
//                    )
//                }
            }
        )

        binding.apply {
            rVMoview.apply {
                adapter = movieAdapter.withLoadStateFooter(
                    MoviesLoadStateAdapter(movieAdapter::retry)
                )
                layoutManager = GridLayoutManager(activity, 3)
                itemAnimator?.changeDuration = 0
            }

            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                viewModel.results.collectLatest { data ->
                    swipeRefreshLayout.isEnabled = true
                    movieAdapter.submitData(data)
                }
            }

            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                movieAdapter.loadStateFlow
                    .distinctUntilChangedBy { it.source.refresh }
                    .filter { it.source.refresh is LoadState.NotLoading }
                    .collect {
                        if (viewModel.pendingScrollToTopAfterNewQuery) {
                            rVMoview.scrollToPosition(0)
                            viewModel.pendingScrollToTopAfterNewQuery = false
                        }
                    }
            }

            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                movieAdapter.loadStateFlow.collect { loadState ->
                    when (val refresh = loadState.mediator?.refresh) {
                        is LoadState.Loading -> {
                            textViewError.isVisible = false
                            buttonRery.isVisible = false
                            textviewNoResults.isVisible = false
                            swipeRefreshLayout.isRefreshing = true
                            rVMoview.showIfOrInvisible {
                                !viewModel.newQueryInProgress && movieAdapter.itemCount > 0
                            }

                            viewModel.refreshInProgress = true
                        }
                        is LoadState.NotLoading -> {
                            textViewError.isVisible = false
                            buttonRery.isVisible = false
                            swipeRefreshLayout.isRefreshing = false
                            rVMoview.isVisible = movieAdapter.itemCount > 0

                            val noResults =
                                movieAdapter.itemCount < 1 && loadState.append.endOfPaginationReached
                                        && loadState.source.append.endOfPaginationReached

                            textviewNoResults.isVisible = noResults

                            viewModel.refreshInProgress = false
                        }
                        is LoadState.Error -> {
                            swipeRefreshLayout.isRefreshing = false
                            textviewNoResults.isVisible = false
                            rVMoview.isVisible = movieAdapter.itemCount > 0

                            val noCachedResults =
                                movieAdapter.itemCount < 1 && !loadState.append.endOfPaginationReached

                            textViewError.isVisible = noCachedResults
                            buttonRery.isVisible = noCachedResults

                            val errorMessage = getString(
                                R.string.could_not_load_search_results,
                                refresh.error.localizedMessage
                                    ?: getString(R.string.unknown_error_occurred)
                            )
                            textViewError.text = errorMessage

                            if (viewModel.refreshInProgress) {
                                showSnackbar(errorMessage)
                            }

                            viewModel.refreshInProgress = false
                        }
                    }
                }
            }

            swipeRefreshLayout.setOnRefreshListener {
                movieAdapter.refresh()
            }

            buttonRery.setOnClickListener {
                movieAdapter.retry()
            }
        }
    }

    private fun initSearchView() {
        binding.searchMovie.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                binding.searchMovie.clearFocus()
                query?.let {
                    viewModel.onSearchQuerySubmit(query)
                } ?: viewModel.onSearchQuerySubmit(null)
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }
        })

        val id = binding.searchMovie.context.resources.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        binding.searchMovie.findViewById<ImageView>(id).setOnClickListener {
            binding.searchMovie.clearFocus()
            binding.searchMovie.setQuery("", false)
            viewModel.onSearchQuerySubmit(null)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        currentBinding = null
    }
}