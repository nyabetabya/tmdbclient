package com.nyabetabya.findmymovie.presentation.movie.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.R
import com.nyabetabya.findmymovie.databinding.FragmentDetailMovieBinding

const val URL_IMAGE = "https://image.tmdb.org/t/p/w500"

class DetailMovieFragment : Fragment(R.layout.fragment_detail_movie) {

    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!

    private lateinit var movie : Movie

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentDetailMovieBinding.bind(view)

        requireArguments().let {
            movie =  it.getParcelable("movie") ?: Movie(0,false,"", "", "","", 1.0, "", "", "", false,1.0, 0)
        }

        initViews()
    }

    private fun initViews() {
        val backdropURL = URL_IMAGE + movie.backdropPath
        val posterURL = URL_IMAGE + movie.posterPath

        Glide.with(requireContext())
            .load(backdropURL)
            .centerCrop()
            .into(binding.imgMovie)

        Glide.with(requireContext())
            .load(posterURL)
            .centerCrop()
            .into(binding.imgPosterMovie)

        binding.apply {
            txtMovieName.text = movie.title
            txtReleaseDate.text = movie.releaseDate
            txtDesc.text = movie.overview

            buttonBack.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}