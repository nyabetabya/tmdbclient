package com.nyabetabya.findmymovie.presentation.movie

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.databinding.ListItemBinding

class MoviesViewHolder(
    private val binding: ListItemBinding,
    private val onItemClick: (Int) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(movie: Movie) {
        binding.apply {
            titleTextView.text = movie.title
            val posterURL = "https://image.tmdb.org/t/p/w500" + movie.posterPath

            Glide.with(imageView.context)
                .load(posterURL)
                .centerCrop()
                .into(imageView)
        }
    }

    init {
        binding.apply {
            root.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION){
                    onItemClick(position)
                }
            }
        }
    }
}