package com.nyabetabya.findmymovie.presentation.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.databinding.ListItemBinding

class MovieAdapter(
    private val onItemClick: (Movie) -> Unit
) : PagingDataAdapter<Movie, MoviesViewHolder>(MovieComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ListItemBinding = ListItemBinding.inflate(layoutInflater)

        return MoviesViewHolder(binding,
            onItemClick = {
                val movie = getItem(it)
                if (movie != null) {
                    onItemClick(movie)
                }
            })
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        val currentItem = getItem(position)

        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }
}
