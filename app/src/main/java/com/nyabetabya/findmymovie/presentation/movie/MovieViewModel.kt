package com.nyabetabya.findmymovie.presentation.movie

import androidx.lifecycle.*
import androidx.paging.cachedIn

import com.nyabetabya.findmymovie.data.repository.movie.MovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject


@HiltViewModel
class MovieViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    private val currentQuery = MutableStateFlow<String?>(null)

    val results = currentQuery.flatMapLatest { query ->
        query?.let {
            repository.getSearchResultsPaged(query)
        } ?: repository.getPopularMoviesPaged()
    }.cachedIn(viewModelScope)

    var refreshInProgress = false
    var newQueryInProgress = false
    var pendingScrollToTopAfterNewQuery = false
    var isAnyQuery = false

    fun onSearchQuerySubmit(query: String?) {
        currentQuery.value = query
        refreshInProgress = true
        pendingScrollToTopAfterNewQuery = true
        isAnyQuery = currentQuery.value != null
    }
}