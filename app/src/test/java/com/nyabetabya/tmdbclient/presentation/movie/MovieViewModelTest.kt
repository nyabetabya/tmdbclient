package com.nyabetabya.tmdbclient.presentation.movie

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.presentation.movie.MovieViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MovieViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MovieViewModel
    //region constants

    //endregion constants

    //region helper fields

    //endregion helper fields


    @Before
    fun setup() {
//        val fakeMovieRepository = FakeMovieRepository()
//        val getMoviesUseCase = GetMoviesUseCase(fakeMovieRepository)
//        val updateMoviesUseCase = UpdateMoviesUseCase(fakeMovieRepository)
//        viewModel = MovieViewModel(getMoviesUseCase,updateMoviesUseCase)

    }

    //region helper methods
    @Test
    fun getMovies_returnsCurrentList(){
        val movies = mutableListOf<Movie>()

        movies.add( Movie(
            1, true, "mature", "es",
            "originalTitle1", "overview1", 4.5,
            "posterPath1", "releaseDate1", "title1",
            true, 4.5, 100
        )
        )

        movies.add( Movie(
            2, true, "mature", "es",
            "originalTitle2", "overview2", 4.5,
            "posterPath2", "releaseDate2", "title2",
            true, 4.5, 100
        ))

//        val currentList = viewModel.getMovies().getOrAwaitValue()
//        assertThat(currentList).isEqualTo(movies)
    }

    @Test
    fun updateMovies_returnsUpdateList(){
        val movies = mutableListOf<Movie>()

        movies.add( Movie(
            3, true, "mature", "es",
            "originalTitle3", "overview3", 4.5,
            "posterPath3", "releaseDate3", "title3",
            true, 4.5, 100
        ))

        movies.add( Movie(
            4, true, "mature", "es",
            "originalTitle4", "overview4", 4.5,
            "posterPath4", "releaseDate4", "title4",
            true, 4.5, 100
        ))

//        val updateList = viewModel.updateMovies().getOrAwaitValue()
//        assertThat(updateList).isEqualTo(movies)
    }
    //endregion helper methods

    //region helper classes

    //endregion helper classes
}