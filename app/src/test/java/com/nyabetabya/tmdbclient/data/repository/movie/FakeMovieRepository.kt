package com.nyabetabya.tmdbclient.data.repository.movie

import com.nyabetabya.findmymovie.data.model.movie.Movie
import com.nyabetabya.findmymovie.data.repository.movie.MovieRepository

class FakeMovieRepository  { //:MovieRepository
    private val movies = mutableListOf<Movie>()

    init {
        movies.add( Movie(
            1, true, "mature", "es",
            "originalTitle1", "overview1", 4.5,
            "posterPath1", "releaseDate1", "title1",
            true, 4.5, 100
        ))

        movies.add( Movie(
            2, true, "mature", "es",
            "originalTitle2", "overview2", 4.5,
            "posterPath2", "releaseDate2", "title2",
            true, 4.5, 100
        ))
    }


//    override suspend fun getMovies(): List<Movie>? {
//        return movies
//    }
//
//    override suspend fun updateMovies(): List<Movie>? {
//        movies.clear()
//        movies.add( Movie(
//            3, true, "mature", "es",
//            "originalTitle3", "overview3", 4.5,
//            "posterPath3", "releaseDate3", "title3",
//            true, 4.5, 100
//        ))
//
//        movies.add( Movie(
//            4, true, "mature", "es",
//            "originalTitle4", "overview4", 4.5,
//            "posterPath4", "releaseDate4", "title4",
//            true, 4.5, 100
//        ))
//
//        return movies
//    }
}